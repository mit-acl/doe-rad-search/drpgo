from pyvitools import *
import PyCeres
import numpy as np
from math import pi, cos, sin
import yaml
import sys
import os
import progressbar
import csv
import random
from copy import deepcopy

# Load simulation run parameters
if len(sys.argv) < 2:
    print('Missing paramfile argument!')
    exit()
with open(sys.argv[1]) as paramfile:
    params = yaml.load(paramfile, Loader=yaml.FullLoader)

# Logging setup
logdir = os.path.dirname(params['csv_logfile'])
if not os.path.exists(logdir):
    os.makedirs(logdir)

# Make reproducible if desired
if params['seed'] != -1:
    np.random.seed(params['seed'])

# TODO ENSURE THAT THE PATHS ARE ALWAYS THE SAME, e.g., generate paths and initial vio a priori

# Single-robot pose graph optimizer
class Robot(object):
    def __init__(self, T_init, sigma_vio, sigma_r, sigma_lc, map_w, map_l):
        # Ceres configuration
        self.pgo = PyCeres.Problem()
        self.options = PyCeres.SolverOptions()
        self.options.max_num_iterations = 50
        self.options.linear_solver_type = PyCeres.LinearSolverType.SPARSE_NORMAL_CHOLESKY
        self.options.minimizer_progress_to_stdout = False
        self.summary = PyCeres.Summary()
        
        # Truth and estimate buffers for thisrobot
        self.x = [T_init]
        self.xhat = [T_init.tocSE3()]
        self.xref = self.xhat
        
        # Add prior belief on initial robot state
        self.pgo.AddParameterBlock(self.xhat[0], 7, PyCeres.cSE3LocalParameterization())
        self.pgo.SetParameterBlockConstant(self.xhat[0])

        # Measurement covariances
        self.sigma_vio = sigma_vio
        self.sigma_r = sigma_r
        self.sigma_lc = sigma_lc

        # Map and controller for random robot walk
        self.T_goal = T_init
        kp = np.array([[0.05, 0.05, 0.05, 0.02, 0.02, 0.05]]).T
        kd = np.array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]).T
        self.controller = PIDController(kp, np.zeros((6,1)), kd, 0.1*np.ones((6,1)))
        self.map_w = map_w
        self.map_l = map_l
        self.z = T_init.t[2,0]

    # Execute random walk at a single time step via PID control on SE3
    def track(self):
        if np.linalg.norm(self.x[-1].t[0:2,0:1] - self.T_goal.t[0:2,0:1]) < 0.5:
            random_dir = np.random.uniform(np.array([[-1.,-1.,0.]]).T, np.array([[1.,1.,0.]]).T)
            random_dir /= np.linalg.norm(random_dir)
            R_rand = SO3.fromTwoUnitVectors(np.array([[1.,0.,0.]]).T, random_dir)
            t_rand = np.random.uniform(np.array([[-self.map_w/2.0,-self.map_l/2.0,self.z]]).T, np.array([[self.map_w/2.0,self.map_l/2.0,self.z]]).T)
            self.T_goal = SE3.fromTranslationAndRotation(t_rand, R_rand)
        self.propagate(self.controller.run(self.x[-1], self.T_goal, 0.01))

    # Take VIO measurement of incremental movement and add to pose graph
    def propagate(self, dT):
        self.x.append(self.x[-1] + dT)
        z_vio = dT + np.random.normal(loc=np.zeros((6,1)), scale=self.sigma_vio)
        self.xhat.append((SE3.fromcSE3(self.xhat[-1]) + z_vio).tocSE3())
        self.pgo.AddParameterBlock(self.xhat[-1], 7, PyCeres.cSE3LocalParameterization())
        self.pgo.AddResidualBlock(PyCeres.cSE3Factor(SE3.Exp(z_vio).tocSE3(), sigma2Q(self.sigma_vio)), None, self.xhat[-2], self.xhat[-1])

    # Determine pose indices for a new simulated loop closure detection
    def addRandomIntraLC(self):
        n = len(self.xhat)
        if n > 2:
            i = np.random.randint(low=0, high=n-1)
            j = np.random.randint(low=0, high=n-2)
            if j == i:
                j = n-1
            self.addIntraLC(i, j)

    # Add loop closure measurement to pose graph
    def addIntraLC(self, i, j):
        z_lc = SE3.Exp(self.x[j] - self.x[i]) + np.random.normal(loc=np.zeros((6,1)), scale=self.sigma_lc)
        self.pgo.AddResidualBlock(PyCeres.cSE3Factor(z_lc.tocSE3(), sigma2Q(self.sigma_lc)), None, self.xhat[i], self.xhat[j])

    # Add inter-agent range measurement (and associated neighbor pose) to pose graph
    def addRange(self, other):
        z_r = np.linalg.norm(other.x[-1].t - self.x[-1].t) + np.random.normal(loc=0.0, scale=self.sigma_r)
        self.pgo.AddParameterBlock(other.xref[-1], 7, PyCeres.cSE3LocalParameterization())
        self.pgo.SetParameterBlockConstant(other.xref[-1])
        self.pgo.AddResidualBlock(PyCeres.RangeFactor(z_r, self.sigma_r**2), None, self.xhat[-1], other.xref[-1])

    # Solve the pose graph at current time step. Updates the values of self.xhat
    def optimize(self):
        PyCeres.Solve(self.options, self.pgo, self.summary)

    # Concise estimate/truth pose representations for CSV logging
    def getLogRow(self, i):
        return self.x[i].tqlist() + SE3.fromcSE3(self.xhat[i]).tqlist()

# Determine the adjacent robots of robot i according to adjacency_graph
def adjacent_indices(adjacency_graph, i):
    a_i = list()
    for idx_set in np.where(adjacency_graph[i,:]>0):
        a_i.extend(list(idx_set))
    return a_i

# Load zero-mean noise stdev values
sigma_vio = np.array([[params['vio_txy_stdev']], 
                      [params['vio_txy_stdev']],
                      [params['vio_tz_stdev']],
                      [params['vio_rp_stdev']],
                      [params['vio_rp_stdev']],
                      [params['vio_yaw_stdev']]])
sigma_lc  = np.array([[params['lc_t_stdev']], 
                      [params['lc_t_stdev']],
                      [params['lc_t_stdev']],
                      [params['lc_r_stdev']],
                      [params['lc_r_stdev']],
                      [params['lc_r_stdev']]])
sigma_r   = params['range_stdev']

# Get map/initialization dimensions and number of robots
x_lower, x_upper = -params['map_width']/2.0, params['map_width']/2.0
y_lower, y_upper = -params['map_length']/2.0, params['map_length']/2.0
z_lower, z_upper = 0.0, params['map_height']
assert(params['num_robots'] > 1)
R_init_circ = params['num_robots'] / pi
dtheta_init = 2.0 * pi / params['num_robots']
h_init = z_lower + (z_upper - z_lower) / 2.0

# Simulate DRPGO
robots = list()
neighbors = list()
anchor_idxs = list()
anchor_idxs_prev = list()
with open(params['csv_logfile'], 'w', newline='') as csvf:
    logfile = csv.writer(csvf, delimiter=',')
    logfile.writerow([params['num_robots'], params['max_comm_dist'], x_lower, x_upper, y_lower, y_upper, z_lower, z_upper])
    for t in progressbar.progressbar(range(params['t_max'])):
        if t == 0:
            # Instantiate robots
            for i in range(params['num_robots']):
                trans_init = np.array([[R_init_circ*cos(i*dtheta_init), R_init_circ*sin(i*dtheta_init), h_init]]).T
                T_init = SE3.fromTranslationAndRotation(trans_init, SO3.identity())
                robots.append(Robot(T_init, sigma_vio, sigma_r, sigma_lc, params['map_width'], params['map_length']))
        else:
            # Simulate robot movement as random walk within map bounds
            for robot in robots:
                robot.track()

            if params['optimize']:
                # Construct communication graph as an adjacency matrix
                # Neighbor robots are within max_comm_dist of each other
                comm_graph = np.eye(params['num_robots'])
                comm_graph_compact = list()
                for i in range(params['num_robots']):
                    i_pos = robots[i].x[-1].t
                    for j in range(i):
                        j_pos = robots[j].x[-1].t
                        if np.linalg.norm(i_pos-j_pos) <= params['max_comm_dist']:
                            comm_graph[i,j] = comm_graph[j,i] = 1.0
                            comm_graph_compact.append((i,j))
                
                if params['anchor']:
                    # Determine anchor(s) and execute anchor optimizations
                    # Compute connected graph components via BFS [Algorithms Illuminated II - Graph Algorithms and Data Structures, 8.3]
                    explored = [False for i in range(params['num_robots'])]
                    numCC = 0
                    CC = dict()
                    for i in range(params['num_robots']):
                        if not explored[i]:
                            numCC += 1
                            Q = [i]
                            while len(Q) > 0:
                                v = Q.pop(0)
                                if not numCC in CC:
                                    CC[numCC] = [v]
                                else:
                                    if not v in CC[numCC]:
                                        CC[numCC].append(v)
                                for w in adjacent_indices(comm_graph, v):
                                    if not explored[w]:
                                        explored[w] = True
                                        Q.append(w)

                    # Within each connected component, choose an anchor and add corrective measurements for each one
                    # Randomly selected unless there was already an anchor agent that's still valid
                    for connected_set in CC.values():
                        found_anchor = False
                        for connected_member in connected_set:
                            if connected_member in anchor_idxs_prev:
                                anchor_idxs.append(connected_member)
                                found_anchor = True
                                break
                        if not found_anchor:
                            anchor_idxs.append(random.choice(connected_set))
                    for anchor_idx in anchor_idxs:
                        for i in range(params['num_anchor_lc']):
                            robots[anchor_idx].addRandomIntraLC()
                        robots[anchor_idx].optimize()
                
                # Share inter-agent information (i.e., range and neighbor poses)
                for i, j in comm_graph_compact:
                    robots[i].addRange(robots[j])
                    robots[j].addRange(robots[i])

                # Optimize pose graphs of all unanchored robots
                for robot in [robots[i] for i in range(params['num_robots']) if not i in anchor_idxs]:
                    robot.optimize()

            anchor_idxs_prev = deepcopy(anchor_idxs)
            anchor_idxs.clear()

        # Log state data for all robots up to current time step
        header_list = ['T', t, len(anchor_idxs_prev)]
        for anchor_idx in anchor_idxs_prev:
            header_list.append(anchor_idx)
        logfile.writerow(header_list)
        for k in range(t+1):
            row = []
            for robot in robots:
                row += robot.getLogRow(k)
            logfile.writerow(row)
