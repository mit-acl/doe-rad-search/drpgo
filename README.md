# Distributed Range-enhanced Pose Graph Optimization

## Repository Structure

- [datasets](datasets)
  - Sensor log information for the dataset tests. Example usage given in the *Dataset Tests* section.
- [param](param)
  - Collection of YAML files for configuring the dataset and simulation tests. More explanation given in the *Dataset Tests* section.
- [pyvitools.py](pyvitools.py)
  - Collection of functions and tools for SO(3)/SE(3) representation, PID control, dataset manipulation, etc. Used by the following three source files.
- [ss\_mr\_pgo.py](ss_mr_pgo.py)
  - Entry point for the dataset tests, in which real or realistic sensor data collected in a CSLAM setting is used in distributed range-enhanced pose graph optimization. Examples given in the *Simulation Tests* section.
- [drpgo.py](drpgo.py)
  - Entry point for the simulation tests, in which communication-subgraphs and agent anchoring are simulated, showcasing the functionality of DRPGO with anchoring. Examples given in the *Simulation Tests* section.
- [results\_vis.ipynb](results_vis.ipynb)
  - Contains plotting functions to view the output log results of the dataset and simulation tests. More information given in the *Usage* section.

## Usage

### Dataset Tests

Four datasets are provided in this repository in the *datasets* folder. Each dataset contains the following:

- True global poses of each agent at each time step.
- VIO estimates from running VINS-Mono run on RGB images and IMU sensor data, with covariances calculated from fitting the errors to a Gaussian distribution.
- Inter- and intra-agent loop closure relative pose measurements from running CCM-SLAM on the RGB and IMU data.
- Altimeter data, spoofed from the truth data with added noise corresponding to the reported covariance.
- Inter-agent range data, spoofed from the truth data with added noise corresponding to the reported covariance.

The individual datasets are:

- **Africa Dataset** (4 Agents)
  - From the AirSim simulation environment.
  - Sparse visual features over a wide map; has the least accurate VIO estimate of all datasets.
  - Parameter files found in *param/Africa*.
- **Euroc Dataset** (3 Agents)
  - From the EuRoC MAV dataset.
  - Small, feature-rich space with accurate VIO estimates.
  - Parameter files found in *param/Euroc*.
- **Lsim Dataset** (4 Agents)
  - From the Flightmare simulation environment.
  - Medium-sized map with sparsely-placed objects and obstacles.
  - Parameter files found in *param/Lsim*.
- **NH Dataset** (4 Agents)
  - From the AirSim simulation environment.
  - Feature-rich, large map. Very little overlap between agent trajectories.
  - Parameter files found in *param/NH*.

Example command to run a dataset test for the Euroc dataset:

```bash
python3 ss_mr_pgo.py param/Euroc/default_alt_and_lc.yaml
```

The parameter files specify where to place the output log files. The log files can be visualized using the following functions in *results\_vis.ipynb*:

- plot_results(): 3D representation of the estimated trajectory for a single agent and time step.
- plot_results_flat(): 6-DOF plots for the estimated trajectory for a single agent and time step.
- plot_RMSE_over_time(): 6-DOF cummulative RMSE plots for all agents and all time steps.

### Simulation Tests

As opposed to the dataset tests, all sensor data are simulated according to the parameter files in *param/DRPGO*. The parameter files also allow for specifying communication sub-graph and anchoring behavior.

Example command to run a simulation test:

```bash
python3 drpgo.py param/DRPGO/full_comm.yaml
```

Results are also output to a CSV log file. Visualization is possible using the following functions in *results\_vis.ipynb*:

- animateDRPGOResults(): 3D animation of all agent trajectory estimates at all time steps.
- plotDRPGOResults(): 6-DOF cummulative RMSE plots for all agents and all time steps.