import sys
from pyvitools import *
import PyCeres
import numpy as np
import matplotlib.pyplot as plt
import yaml
import os
import progressbar
import csv
from copy import deepcopy

with open(sys.argv[1]) as paramfile:
    params = yaml.load(paramfile, Loader=yaml.FullLoader)

## truth/estimate/factor buffers
stats = dict()
x = dict()
xhat = dict()
between_factors = dict()
intra_loop_factors = dict()
inter_loop_factors = dict()
range_factors = dict()
alt_factors = dict()

print('Loading dataset...')

dataset = MultiAgentPGODataset()
dataset.read(params['dataset'])

for r in range(dataset.num_robots):
    stats[r] = dict()
    x[r] = dict()
    xhat[r] = dict()
    between_factors[r] = dict()
    intra_loop_factors[r] = dict()
    inter_loop_factors[r] = dict()
    range_factors[r] = dict()
    alt_factors[r] = dict()

## add true poses for each robot
for r, p, T in dataset.true_poses:
    x[r][p] = T

## differentiate factors for each robot
for r, pi, pj, T, cov in dataset.between_factors:
    between_factors[r][(pi, pj)] = (T, params['odom_cov_kappa'] * cov * np.eye(6))

for r, pi, pj, T, cov in dataset.intra_loop_factors:
    intra_loop_factors[r][(pi, pj)] = (T, params['intra_loop_cov_kappa'] * cov * np.eye(6))

for ri, rj, pi, pj, T, cov in dataset.inter_loop_factors: # shared poses
    inter_loop_factors[ri][(rj, pi, pj)] = (T, params['inter_loop_cov_kappa'] * cov * np.eye(6))
    inter_loop_factors[rj][(ri, pj, pi)] = (T.inverse(), params['inter_loop_cov_kappa'] * cov * np.eye(6))

for ri, rj, pi, pj, d, cov in dataset.range_factors: # shared poses
    range_factors[ri][(rj, pi, pj)] = (d, params['range_kappa'] * cov)
    range_factors[rj][(ri, pj, pi)] = (d, params['range_kappa'] * cov)

for r, p, h, cov in dataset.alt_factors:
    alt_factors[r][p] = (h, params['alt_cov_kappa'] * cov)

## compile statistics for distributed PGO simulation
for r in range(dataset.num_robots):
    stats[r]['n'] = len(x[r].keys()) # total number of poses
    # simulate ego motion estimate from VIO
    for k in range(stats[r]['n']):
        if k == 0 or r == params['anchor_idx']:
            xhat[r][k] = x[r][k].tocSE3()
        else:
            xhat[r][k] = (SE3.fromcSE3(xhat[r][k-1]) * between_factors[r][(k-1,k)][0]).tocSE3()

## setup logging
if not os.path.exists(params['logdir']):
    os.makedirs(params['logdir'])
def log(r, t, flag=None):
    flag = ('t%d' % t if flag is None else flag)
    with open(os.path.join(params['logdir'], '%d_%s.csv' % (r, flag)), 'w', newline='') as csvf:
        writer = csv.writer(csvf, delimiter=',')
        for i in range(min(stats[r]['n'], t)):
            writer.writerow(x[r][i].tqlist() + SE3.fromcSE3(xhat[r][i]).tqlist())

print('done.')

## log initial VIO estimates, prior to optimization
t_max = max([stats[r]['n'] for r in range(dataset.num_robots)])
for r in range(dataset.num_robots):
    log(r, t_max, 'initial')

## simulate DPGO problem at each time step iteration
#  shared poses are communicated automatically via pointers
print('SOLVING DISTRIBUTED PGO...')
for t in progressbar.progressbar(range(t_max)):
    if t > 0 and (t % params['optimize_every_n_poses'] == 0 or t == t_max-1):
        for r in range(dataset.num_robots):
            if r != params['anchor_idx']:
                problem = PyCeres.Problem()
                neighbor_dvs = list()

                p_lc_indices = deepcopy(list(intra_loop_factors[r].keys()))
                s_lc_indices = deepcopy(list(inter_loop_factors[r].keys()))
                s_range_indices = deepcopy(list(range_factors[r].keys()))

                for k in range(t):
                    if k == 0:
                        # fix initial pose for this robot
                        problem.AddParameterBlock(xhat[r][k], 7, PyCeres.cSE3LocalParameterization())
                        problem.SetParameterBlockConstant(xhat[r][k])
                        continue
                    
                    if k < stats[r]['n']:
                        # add decision variable
                        problem.AddParameterBlock(xhat[r][k], 7, PyCeres.cSE3LocalParameterization())

                        # add odometry measurement
                        z_odom, o_odom = between_factors[r][(k-1,k)]
                        problem.AddResidualBlock(PyCeres.cSE3Factor(z_odom.tocSE3(), o_odom), None, xhat[r][k-1], xhat[r][k])

                        # add altitude measurement
                        z_alt, o_alt = alt_factors[r][k]
                        problem.AddResidualBlock(PyCeres.AltFactor(z_alt, o_alt), None, xhat[r][k])

                        # add private loop closure measurement
                        for i, j in p_lc_indices:
                            if k == max(i,j):
                                z_lc, o_lc = intra_loop_factors[r][(i,j)]
                                problem.AddResidualBlock(PyCeres.cSE3Factor(z_lc.tocSE3(), o_lc), None, xhat[r][i], xhat[r][j])
                                p_lc_indices.remove((i,j))

                    # add shared loop closure measurement
                    for other_r, i, other_i in s_lc_indices:
                        if k == max(i, other_i):
                            z_lc, o_lc = inter_loop_factors[r][(other_r,i,other_i)]
                            if not (other_r, other_i) in neighbor_dvs:
                                problem.AddParameterBlock(xhat[other_r][other_i], 7, PyCeres.cSE3LocalParameterization())
                                problem.SetParameterBlockConstant(xhat[other_r][other_i])
                                neighbor_dvs.append((other_r, other_i))
                            problem.AddResidualBlock(PyCeres.cSE3Factor(z_lc.tocSE3(), o_lc), None, xhat[r][i], xhat[other_r][other_i])
                            s_lc_indices.remove((other_r,i,other_i))

                    # add shared range measurement
                    for other_r, i, other_i in s_range_indices:
                        if k == max(i, other_i):
                            z_range, o_range = range_factors[r][(other_r,i,other_i)]
                            if not (other_r, other_i) in neighbor_dvs:
                                problem.AddParameterBlock(xhat[other_r][other_i], 7, PyCeres.cSE3LocalParameterization())
                                problem.SetParameterBlockConstant(xhat[other_r][other_i])
                                neighbor_dvs.append((other_r, other_i))
                            problem.AddResidualBlock(PyCeres.RangeFactor(z_range, o_range), None, xhat[r][i], xhat[other_r][other_i])
                            s_range_indices.remove((other_r,i,other_i))

                # solve problem!
                options = PyCeres.SolverOptions()
                options.max_num_iterations = 50
                options.linear_solver_type = PyCeres.LinearSolverType.SPARSE_NORMAL_CHOLESKY
                options.minimizer_progress_to_stdout = False
                summary = PyCeres.Summary()
                PyCeres.Solve(options, problem, summary)

            # log solution
            log(r, t)

# log final
for r in range(dataset.num_robots):
    log(r, t_max, 'final')

print('DONE. Results in %s.' % params['logdir'])
